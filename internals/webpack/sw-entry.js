self.addEventListener('fetch', function (event) {
  const url = new URL(event.request.url);
  url.hash = '';
  let urlString = url.toString();
  let cacheUrl = urlString;
  let CACHE_PREFIX = 'itune_data_';
  let CACHE_TAG = 'v1';
  let CACHE_NAME = CACHE_PREFIX + ':' + CACHE_TAG;
  let resource = undefined;

  function cachesMatch (request, cacheName) {
    return caches.match(request, {
      cacheName: cacheName
    }).then(function (response) {
      return response
    })
    // Return void if error happened (cache not found)
    ['catch'](function () {})
  }
  function cacheFirst(cacheUrl, CACHE_NAME) {
    const resource = cachesMatch(cacheUrl, CACHE_NAME).then(function (response) {
      if (response) {
        return response;
      }
      // Load and cache known assets
      const fetching = fetch(urlString).then(function (response) {
        if (!response.ok) {
          return response;
        }
        (function () {
          const responseClone = response.clone();
          const storing = caches.open(CACHE_NAME).then(function (cache) {
            return cache.put(urlString, responseClone);
          }).then(function () {
            console.log('[SW]:', 'Cache asset: ' + urlString);
          });
          event.waitUntil(storing);
        })();

        return response;
      });

      return fetching;
    })
    return resource;
  }
  function netWorkFirst(cacheUrl, CACHE_NAME) {
    const resource = fetch(cacheUrl).then(response => {
      if (response.ok) {
        const responseClone = response.clone()
        const storing = caches.open(CACHE_NAME).then(function (cache) {
          cache.put(cacheUrl, responseClone);
        }).then(function () {
          console.log('[SW]:', 'Cache asset: ' + cacheUrl);
        });
        event.waitUntil(storing);
        return response;
      }
      // Throw to reach the code in the catch below
      throw new Error('Response is not ok');
    })
    ['catch'](function () {
      return cachesMatch(cacheUrl, CACHE_NAME);
    });
    return resource;
  }

  resource = cacheFirst(cacheUrl, CACHE_NAME);
  event.respondWith(resource);
})
