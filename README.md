## Description

This project is for offsite testing.

## Installation

```
yarn install
```

## Run Command

This command is for building the files for production server.
```
yarn run build
```

You can use this command in order to clear exist build.
```
yarn run build:clean
```

Start your localhost server in develop environment.
```
yarn run start
```

Start production server with production enviroment
```
yarn run start:prod
```

## Remark
this project include "offsite-plugin" which connect servicework for user to visit the site in offline mode.