import styled from 'styled-components';

export const DetailCardWrapper = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 15;
    background-color: rgba(255,255,255,.3);
`;

export const DetailCardBlock = styled.div`
    position: absolute;
    box-shadow: 0 0 43px #ccc;
    top: -100%;
    left: 50%;
    margin-top: -250px;
    margin-left: -200px;
    width: 100%;
    max-width: 400px;
    height: 100%;
    max-height: 500px;
    overflow-x: hidden;
    overflow-y: auto;
    -webkit-overflow-scrolling: touch;
    background-color: #fff;
    border-radius: 10px;
    padding: 12px;
    box-sizing: border-box;
    transition: all .8s;

    > div:not(#close-btn) {
        width: 100%;
    }

    div:after{
        left: 88px;
    }

    #close-btn{
        position:absolute;
        top:7px;
        right:7px;
        cursor: pointer;
    }

    @media only screen and (max-width: 700px) {
        margin-left: -40vw;
        max-width: 80vw;
    }

    &.ready{
        top: 50%;
    }
`;
