import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import AppItem from 'components/AppItem';
import { DetailCardWrapper, DetailCardBlock } from './index.style';

class DetailCard extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      ready: false,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({ ready: true });
    }, 400);
  }

  onClose = () => {
    const { onClose } = this.props;

    this.setState({ ready: false });
    setTimeout(() => {
        onClose();
    }, 400);
  }

  render() {
    const { ready } = this.state;
    const { item } = this.props;

    return (
      <DetailCardWrapper>
        <DetailCardBlock className={classNames({ ready })}>
          <AppItem item={item} noPerspective />
          <br />
          <br />
          {item.name}
          <br />
          {item.category}
          <br />
          Price: {item.detail.formattedPrice}
          <br/>
          {item.detail.description}
          <div id="close-btn" onClick={this.onClose}>
            <i className="material-icons">close</i>
          </div>
        </DetailCardBlock>
      </DetailCardWrapper>
    );
  }
}

DetailCard.propTypes = {
  item: PropTypes.object,
};

export default DetailCard;
