import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Truncate from 'react-truncate';
import Rating from 'react-rating';

import AppIcon from '../AppIcon';
import {
  AppItemBlockWrapper, AppItemBlock, AppItemName,
  RatingWrapper,
} from './index.style';

class AppItem extends React.PureComponent {
  render() {
    const {
      item, circle, horizon, idx,
      onClick, noPerspective,
      isVisible, style,
    } = this.props;

    return (
      <div className="app-item" style={style}>
        <AppItemBlockWrapper className={classNames({ 'list-perspective': !horizon && !noPerspective })} horizon={horizon} idx={idx}>
          <AppItemBlock className={classNames('list-perspective', { loaded: !horizon && isVisible })} horizon={horizon} idx={idx}>
            {
              (idx || idx === 0) && !horizon ? <div className="rank-number">{idx + 1}</div> : null
            }
            <AppIcon circle={circle} img={item.image.url} horizon={horizon} onClick={onClick} />
            <AppItemName horizon={horizon}>
              {
                horizon
                  ? (
                    <Truncate width={80} lines={2} ellipsis={<span>...</span>}>
                      {item.name}
                    </Truncate>
                  ) : item.name
              }
              <div className="category">{item.category}</div>
              {
                (idx || idx === 0) && !horizon
                  ? (
                    <RatingWrapper>
                      <Rating
                        initialRating={item.detail.averageUserRating}
                        fullSymbol={<i className="material-icons">star</i>}
                        emptySymbol={<i className="material-icons">star_border</i>}
                        readonly
                      />
                      {
                        item.detail.userRatingCount
                          ? (
                            <span className="rating-count">
                              ({item.detail.userRatingCount})
                            </span>
                          ) : null
                      }
                    </RatingWrapper>
                  ) : null
              }
            </AppItemName>
          </AppItemBlock>
        </AppItemBlockWrapper>
      </div>
    );
  }
}

AppItem.propTypes = {
  idx: PropTypes.number,
  item: PropTypes.object,
  style: PropTypes.object,
  circle: PropTypes.bool,
  isVisible: PropTypes.bool,
  noPerspective: PropTypes.bool,
  horizon: PropTypes.bool,
  onClick: PropTypes.func,
};

export default AppItem;
