import styled from 'styled-components';

export const AppItemBlockWrapper = styled.div`
    position:relative;
    display: flex;
    width: ${props => props.horizon ? 'calc(100% / 10)' : '100%'};
    padding: 8px;
    box-sizing: border-box;

    &.list-perspective{
        perspective: 400px;
        perspective-origin: 50% 50%;
    }

    &::after{
        position: absolute;
        left: 96px;
        bottom: 0;
        display: ${props => props.horizon ? 'none' : 'inline'};
        content: ' ';

        width: calc(100% - 94px);
        height: 1px;
        background-color:#ccc;
    }

    @media only screen and (max-width:700px){
        width: ${props => props.horizon ? 'calc(100% / 10)' : '100%'};
    }
`;

export const AppItemBlock = styled.div`
    width: ${props => props.horizon ? 'auto' : '100%'};
    display: ${props => props.horizon ? 'inline-block' : 'flex'};
    align-items: center;

    &.list-perspective{
        transform-style: preserve-3d;
        perspective-origin: 25% 75%;
        transform: translate3d(0,0,80px);
        transition: transform .5s;
    }

    &.loaded{
        transform: translate3d(0,0,0);
    }

    .rank-number{
        padding-right: 8px;
    }
`;

export const AppItemName = styled.div`
    padding: ${props => props.horizon ? '0' : '0 8px'};
    box-sizing:border-box;
    width: ${props => props.horizon ? 'auto' : 'calc(100% - 80px)'};

    .category{
        font-size: 14px;
        color: #444;
    }
`;

export const RatingWrapper = styled.span`
    i{
        color: #ff9400;
        font-size: 14px;
    }

    .rating-count{
        position:relative;
        top: -2px;
        margin-left: 4px;
        color: #666;
        font-size: 12px;
    }
`;
