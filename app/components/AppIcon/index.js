import React from 'react';
import PropTypes from 'prop-types';
import { AppThumb } from './index.style';

const AppIcon = ({ img, circle, onClick }) => (
  <AppThumb circle={circle}>
    <img src={img} alt="" width="100%" onClick={onClick} />
  </AppThumb>
);

AppIcon.propTypes = {
  img: PropTypes.string,
  circle: PropTypes.bool,
  onClick: PropTypes.func,
};

export default AppIcon;
