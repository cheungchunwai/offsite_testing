import styled from 'styled-components';

export const AppThumb = styled.div`
    width: ${props => props.horizon ? '80px' : '72px'};
    height: ${props => props.horizon ? '80px' : '70px'};
    overflow: hidden;
    border-radius: ${props => props.circle ? '80px' : '10px'};

    &:hover{
        cursor: pointer;
    }
`;
