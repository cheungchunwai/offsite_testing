import styled from 'styled-components';

const H1 = styled.h1`
  font-size: 1.5em;
  margin: 0.25em;
`;

export default H1;
