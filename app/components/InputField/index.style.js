import styled from 'styled-components';

export const InputWrapper = styled.div`
    width: 100%;
    padding: 8px;
    box-sizing: border-box;
    z-index: 10;
    background-color: #fff;

    .input-box{
        position: relative;

        .input-label{
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            cursor: text;

            &:hover{
                opacity: .3;
            }
        }
    }

    input{
        width: 100%;
        padding: 12px;
        box-sizing: border-box;
        font-size: 14px;
        background-color: #eee;
        border: 0;
        outline: 0;
    }
`;