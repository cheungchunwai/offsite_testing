import React from 'react';
import PropTypes from 'prop-types';
import { InputWrapper } from './index.style';

class InputField extends React.PureComponent {
  constructor() {
    super();

    this.state = {
        keyword:'',
    };
  }

  onChange = (val) => {
      const { onChange } = this.props;

      this.setState({ keyword:val });
      onChange(val);
  }

  render() {
    const { keyword } = this.state;
    const { onChange } = this.props;
    return (
      <InputWrapper>
        <div className="input-box">
          <input ref={instance => this.input = instance} type="text" onChange={e => this.onChange(e.target.value)} value={keyword}/>
          {
              keyword.length == 0
              ? (
                <div className="input-label" onClick={() => { this.input.focus(); }}>
                    <i className="material-icons">search</i>
                    搜尋
                </div>
              ) : null
          }
        </div>
      </InputWrapper>
    );
  }
}

InputField.propTypes = {
  onChange: PropTypes.func,
};

export default InputField;
