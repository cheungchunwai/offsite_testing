/*
 * App Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import { LOAD_REPOS, UPDATE_REPOS, LOAD_REPOS_SUCCESS, LOAD_REPOS_ERROR } from './constants';

/**
 * Load the data from itunes, this action starts the request saga
 *
 * @return {object} An action object with a type of LOAD_REPOS
 */
export function loadRepos() {
  return {
    type: LOAD_REPOS,
  };
}

/**
 * Load the data from itunes, this action starts the request saga
 *
 * @return {object} An action object with a type of LOAD_REPOS
 */
export function updateRepos(idx, item) {
  return {
    type: UPDATE_REPOS,
    updateItem: item,
    idx: idx,
  };
}

/**
 * Dispatched when the  data from itunes are loaded by the request saga
 *
 * @param  {array} top10App Top Ten data
 * @param  {array} topFreeApp Top Free App Data
 *
 * @return {object}      An action object with a type of LOAD_REPOS_SUCCESS passing the repos
 */
export function reposLoaded(top10App, topFreeApp) {
  return {
    type: LOAD_REPOS_SUCCESS,
    top10App,
    topFreeApp,
  };
}

/**
 * Dispatched when loading the repositories fails
 *
 * @param  {object} error The error
 *
 * @return {object}       An action object with a type of LOAD_REPOS_ERROR passing the error
 */
export function repoLoadingError(error) {
  return {
    type: LOAD_REPOS_ERROR,
    error,
  };
}
