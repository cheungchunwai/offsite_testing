import styled from 'styled-components';

export const FixedWrapper = styled.div`
    position: fixed;
    z-index: 10;
    left: 0;
    top: 0;
    width: 100%;
    background-color: #fff;
`;


export const FixedBlock = styled.div`
    max-width: 1024px;
    margin: auto;
`;

export const HorizonList = styled.div`
    width: 100%;
    overflow-y: hidden;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;

    > div {
        display: table;

        > div {
            display: table-cell;
        }
    }
`;

export const Wrapper = styled.div`
    position:relative;
    padding-top: ${props => props.topTenApp ? '315px' : '100px'};
    max-width: 1024px;
    margin: auto;
    transition: opacity .8s;

    &.blur{
        opacity: .3;
    }

    #detect{
        width: 200px;
        margin:16px auto;
        padding:15px 0;
        opacity: 1;
        background-color: #ddd;
        text-align: center;
        border-radius: 8px;

        &:hover{
            background-color: #ccc;
            cursor: pointer;
        }
    }

    .autoSizerWrapper{    
        position: relative;
        -webkit-box-flex: 1;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
    }
`;
