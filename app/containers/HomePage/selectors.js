/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectHome = state => state.get('home', initialState);

const makeSelectTopTenAppList = () => createSelector(selectHome, homeState => homeState.get('topTenAppList').toJS());
const makeSelectTopFreeAppList = () => createSelector(selectHome, homeState => homeState.get('topFreeAppList').toJS());
const makeSelectAppDetailList = () => createSelector(selectHome, homeState => homeState.get('appDetailList').toJS());
const makeSelectKeyword = () => createSelector(selectHome, homeState => homeState.get('keyword'));
const makeSelectListLoading = () => createSelector(selectHome, homeState => homeState.get('loading'));
const makeSelectPage = () => createSelector(selectHome, homeState => homeState.get('page'));
const makeSelectLimit = () => createSelector(selectHome, homeState => homeState.get('limit'));
const makeSelecthasNext = () => createSelector(selectHome, homeState => homeState.get('hasNext'));

export {
  selectHome, makeSelectTopTenAppList, makeSelectTopFreeAppList, makeSelectKeyword,
  makeSelecthasNext, makeSelectAppDetailList, makeSelectListLoading, makeSelectPage, makeSelectLimit,
};
