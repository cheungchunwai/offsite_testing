/*
 * HomeConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const FETCH_CACHE_LIST = 'offsite/Home/FETCH_CACHE_LIST';
export const SLICE_CACHE_LIST = 'offsite/Home/SLICE_CACHE_LIST';
export const SEARCH_CACHE = 'offsite/Home/SEARCH_CACHE';
