/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */
import { fromJS } from 'immutable';
import {
  FETCH_CACHE_LIST,
  SLICE_CACHE_LIST,
  SEARCH_CACHE,
} from './constants';

// The initial state of the App
export const initialState = fromJS({
  page: 0,
  loading: false,
  limit: 10,
  keyword: '',
  topTenAppList: [],
  topFreeAppList: [],
  appDetailList: [],
  hasNext: true,
});

function homeReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_CACHE_LIST:
      return state
        .set('loading',true);
    case SLICE_CACHE_LIST:
      return state
        .set('hasNext', action.cacheTopFreeData.size >= state.get('limit') * (state.get('page') + 1))
        .set('loading', false)
        .set('page', state.get('page') + 1)
        .set('topTenAppList', action.cacheTopTenData)
        .set('topFreeAppList', action.cacheTopFreeData);
    case SEARCH_CACHE:
      return state
        .set('page', 0)
        .set('topTenAppList', fromJS([]))
        .set('topFreeAppList', fromJS([]))
        .set('keyword', action.keyword);
    default:
      return state;
  }
}

export default homeReducer;
