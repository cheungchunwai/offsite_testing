/*
 * HomePage
 *
 * List all the features
 */
import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import classNames from 'classnames';
import { WindowScroller, AutoSizer, List, Grid } from 'react-virtualized';
import 'react-virtualized/styles.css';
import { createStructuredSelector } from 'reselect';

import {
  makeSelectLoading,
  makeSelectError,
} from 'containers/App/selectors';


import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';

import AppItem from 'components/AppItem';
import H1 from 'components/H1';
import InputField from 'components/InputField';
import DetailCard from 'components/DetailCard';
import LoadingIndicator from 'components/LoadingIndicator';

import { loadRepos } from '../App/actions';
import { loadCache, searchCache } from './actions';
import { makeSelectTopTenAppList, makeSelectTopFreeAppList, makeSelecthasNext, makeSelectListLoading, makeSelectPage } from './selectors';
import reducer from './reducer';
import saga from './saga';

import { FixedWrapper, FixedBlock, HorizonList, Wrapper } from './index.style';

export class HomePage extends React.Component {
  constructor() {
    super();
    this.state = {
      loadMore: false,
      currentItem: false,
    };

    this.windowScroller
  }

  componentDidMount() {
    const { onFetchDataFromItunes } = this.props;

    onFetchDataFromItunes();
  }

  componentDidUpdate() {
    const { loading, lazyAppList, onFetchDataFromCache } = this.props;

    if (!loading && lazyAppList.length === 0) {
      onFetchDataFromCache();
    }
  }

  handleScroll = ({ scrollTop }) => {
    const body = document.body, html = document.documentElement;
    const contentHeight = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight );
    const ratio =  scrollTop/(contentHeight - document.documentElement.clientHeight - this.offsetPanel.getBoundingClientRect().height);
    console.log(ratio);
    this.topScrollPanelWrapper.scrollLeft = (this.topScrollPanel.getBoundingClientRect().width - document.documentElement.clientWidth) * ratio;
  }

  showDetail = (item) => {
    this.setState({ currentItem: item });
  }

  rowRenderer = ({index, isVisible, key, style}) => {
    const { lazyAppList } = this.props;
    const item = lazyAppList[index];

    return (
      <AppItem
       key={key}
       item={item}
       circle={index % 2 !== 0}
       idx={index}
       onClick={(e) => this.showDetail(item)}
       style={style}
       isVisible={isVisible}
      />
    )
  }

  cellRenderer = ({ columnIndex, isVisible, key, rowIndex, style }) => {
    const { lazyAppList } = this.props;
    const index = columnIndex + rowIndex + rowIndex;
    const item = lazyAppList[index];

    return (
      <AppItem
       key={key}
       item={item}
       circle={index % 2 !== 0}
       idx={index}
       onClick={(e) => this.showDetail(item)}
       style={style}
       isVisible={isVisible}
      />
    )
  }
  noContentRenderer = () => {
    return <div>No cells</div>;
  }

  render() {
    const { currentItem } = this.state;
    const {
      error, page, loading, listLoading, topTenApp, lazyAppList, onSearch, hasNext, onFetchDataFromCache,
    } = this.props;

    return (
      <div>
        <Helmet>
          <title>Home Page</title>
          <meta
            name="description"
            content="HomePage"
          />
        </Helmet>
        {
          !loading
            ? (
              <Wrapper
                id="wrapper"
                className={classNames({ blur: currentItem })}
                topTenApp={topTenApp && topTenApp.length}
              >
                <FixedWrapper>
                  <FixedBlock ref={instance => this.offsetPanel = instance}>
                    <InputField onChange={onSearch} />
                    {
                      topTenApp && topTenApp.length
                        ? <H1>推介</H1>
                        : null
                    }
                    <HorizonList ref={instance => this.topScrollPanelWrapper = instance}>
                      <div ref={instance => this.topScrollPanel = instance}>
                        {
                          topTenApp
                            ? (
                              topTenApp.map((item, idx) => (
                                <AppItem
                                  key={item.id}
                                  item={item}
                                  horizon
                                  idx={idx}
                                  onClick={() => this.showDetail(item)}
                                />))
                            ) : null
                        }
                      </div>
                    </HorizonList>
                    {
                      lazyAppList && lazyAppList.length
                        ? <H1>免費排行</H1>
                        : null
                    }
                  </FixedBlock>
                </FixedWrapper>
                {
                  lazyAppList && lazyAppList.length
                    ? (
                        <WindowScroller 
                          onScroll={this.handleScroll}
                        >
                          {({ height, isScrolling, registerChild, onChildScroll, scrollTop }) => (

                            <div ref={(instance) => this.windowScroller = instance} className="autoSizerWrapper">
                              <AutoSizer disableHeight>
                                {({width}) => (
                                  <div ref={registerChild}>
                                  {
                                    width < 700
                                    ? (
                                      <List
                                        autoHeight
                                        height={height}
                                        isScrolling={isScrolling}
                                        onScroll={onChildScroll}
                                        rowCount={lazyAppList.length}
                                        rowHeight={80}
                                        overscanRowCount={4}
                                        rowRenderer={this.rowRenderer}
                                        scrollTop={scrollTop}
                                        width={width}
                                      />
                                    ) : (
                                      <Grid
                                        cellRenderer={this.cellRenderer}
                                        noContentRenderer={this.noContentRenderer}
                                        columnWidth={this.windowScroller ? this.windowScroller.getBoundingClientRect().width/2 : 512}
                                        columnCount={2}
                                        height={height/2 - 52}
                                        overscanRowCount={4}
                                        rowHeight={80}
                                        rowCount={Math.floor(lazyAppList.length/2)}
                                        width={width}
                                      />
                                    )
                                  }
                                  </div>
                                  )}
                            </AutoSizer>
                            {
                              hasNext && !listLoading
                                ? (
                                  <div
                                    id="detect"
                                    ref={(instance) => this.detect = instance}
                                    onClick={() => onFetchDataFromCache()}
                                  >
                                    更多
                                  </div>
                                ) : null
                            }
                          </div>
                          )}
                        </WindowScroller>
                    ) : null
                }
                {
                  listLoading 
                  ? <LoadingIndicator />
                  : null
                }
                {error}
              </Wrapper>
            ) : <LoadingIndicator />
        }
        {
          currentItem
            ? (
              <DetailCard
                item={currentItem}
                onClose={() => {
                  this.setState({ currentItem: false });
                }}
              />
            ) : null
        }
      </div>
    );
  }
}

HomePage.propTypes = {
  page: PropTypes.number,
  loading: PropTypes.bool,
  listLoading: PropTypes.bool,
  hasNext: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  topTenApp: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
  lazyAppList: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
  onFetchDataFromItunes: PropTypes.func,
  onFetchDataFromCache: PropTypes.func,
  onSearch: PropTypes.func,
};

export function mapDispatchToProps(dispatch) {
  return {
    onFetchDataFromItunes: () => {
      dispatch(loadRepos());
    },
    onFetchDataFromCache: () => {
      dispatch(loadCache());
    },
    onSearch: (keyword) => {
      dispatch(searchCache(keyword));
    },
  };
}

const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoading(),
  page: makeSelectPage(),
  lazyAppList: makeSelectTopFreeAppList(),
  topTenApp: makeSelectTopTenAppList(),
  listLoading: makeSelectListLoading(),
  error: makeSelectError(),
  hasNext: makeSelecthasNext(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'home', reducer });
const withSaga = injectSaga({ key: 'home', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(HomePage);
