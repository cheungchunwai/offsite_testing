/*
 * App Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import { FETCH_CACHE_LIST, SLICE_CACHE_LIST, SEARCH_CACHE } from './constants';


export function loadCache() {
  return {
    type: FETCH_CACHE_LIST,
  };
}

export function slicedCache(cacheTopTenData, cacheTopFreeData) {
  return {
    type: SLICE_CACHE_LIST,
    cacheTopTenData,
    cacheTopFreeData,
  };
}

export function searchCache(keyword) {
  return {
    type: SEARCH_CACHE,
    keyword,
  };
}
