/**
 * Gets Data from itunes
 */

import {
  all, call, select, put, takeLatest,
} from 'redux-saga/effects';
import { fromJS } from 'immutable';
import request from 'utils/request';

import { reposLoaded, updateRepos, repoLoadingError } from 'containers/App/actions';
import { makeSelectTopFreeApp, makeSelectTopTenApp } from 'containers/App/selectors';
import { LOAD_REPOS } from 'containers/App/constants';

import { FETCH_CACHE_LIST, SEARCH_CACHE } from './constants';
import { slicedCache } from './actions';
import { makeSelectKeyword, makeSelectPage, makeSelectLimit, makeSelecthasNext } from './selectors';

function regroupObjArr(arr) {
  let result = [];
  result = arr.map(item => ({
    id: item.id.attributes['im:id'],
    name: item['im:name'].label,
    category: item.category.attributes.label,
    author: item['im:artist'].label,
    image: {
      url: item['im:image'][2].label,
      height: item['im:image'][2].attributes.height,
    },
    summary: item.summary.label,
  }));
  return result;
}

/**
 * fetch data from itunes request/response handler
 */
export function* getRepos() {
  const requestTop10AppURL = 'https://itunes.apple.com/hk/rss/topgrossingapplications/limit=10/json';
  const requestTopFreeAppURL = 'https://itunes.apple.com/hk/rss/topfreeapplications/limit=100/json';

  try {
    const [top10App, topFreeApp] = yield all([
      call(request, requestTop10AppURL),
      call(request, requestTopFreeAppURL),
    ]);

    const tmptopTenApp = regroupObjArr(top10App.feed.entry);
    const tmptopFreeApp = regroupObjArr(topFreeApp.feed.entry);

    for (const i in tmptopTenApp) {
      if (tmptopTenApp.hasOwnProperty(i)) {
        const requestDetailURL = `https://itunes.apple.com/hk/lookup?id=${tmptopTenApp[i].id}`;
        const detailData = yield call(request, requestDetailURL, {
          crossDomain: true,
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
        });
        tmptopTenApp[i].detail = detailData.results[0];
      }
    }

    yield put(reposLoaded(
      top10App ? fromJS(tmptopTenApp) : [],
      topFreeApp ? fromJS(tmptopFreeApp) : [],
    ));
  } catch (err) {
    yield put(repoLoadingError(err));
  }
}

/**
 * filterCache handler
 */

export function filterCacheData(keyword, cacheData) {
  const tmpKeyword = keyword.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
  
  return fromJS(cacheData).filter(val => (
    val.getIn(['name']).search(new RegExp(tmpKeyword, 'i')) >= 0
      || val.getIn(['author']).search(new RegExp(tmpKeyword, 'i')) >= 0
      || val.getIn(['category']).search(new RegExp(tmpKeyword, 'i')) >= 0
      || val.getIn(['summary']).search(new RegExp(tmpKeyword, 'i')) >= 0
  ));
}

export function* filterCache() {
  try {
    const page = yield select(makeSelectPage());
    const limit = yield select(makeSelectLimit());
    const keyword = yield select(makeSelectKeyword());
    const cacheTopTenAppData = yield select(makeSelectTopTenApp());
    let cacheTopFreeAppData = yield select(makeSelectTopFreeApp());
    const tmpKeyword = keyword.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");

    cacheTopFreeAppData = cacheTopFreeAppData.slice(0, limit * (page + 1));

    for (const i in cacheTopFreeAppData) {
      if (!cacheTopFreeAppData[i].hasOwnProperty('detail')) {
        const requestDetailURL = `https://itunes.apple.com/hk/lookup?id=${cacheTopFreeAppData[i].id}`;
        const detailData = yield call(request, requestDetailURL, {
          crossDomain: true,
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
        });
        cacheTopFreeAppData[i].detail = detailData.results[0];
        yield put(updateRepos( i, cacheTopFreeAppData[i]));
      }
    }

    const filteredTopFreeAppCache = filterCacheData(tmpKeyword, cacheTopFreeAppData);
    const filteredTopTenAppCache = filterCacheData(tmpKeyword, cacheTopTenAppData);

    yield put(slicedCache(filteredTopTenAppCache, filteredTopFreeAppCache));
  } catch (err) {
    yield put(repoLoadingError(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* appStoreData() {
  yield takeLatest(LOAD_REPOS, getRepos);
  yield takeLatest(FETCH_CACHE_LIST, filterCache);
  yield takeLatest(SEARCH_CACHE, filterCache);
}
